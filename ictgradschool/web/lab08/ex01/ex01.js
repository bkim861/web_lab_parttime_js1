"use strict";

// TODO Declare variables here
var myFirstNumber = 3;
var myFirstBoolean = false;
var mySecondNumber = 10.25;
var myFirstString = "Hello";
var mySecondString = "World";

var a = myFirstNumber + 7;
var b = mySecondNumber - myFirstNumber
var helloWorld = myFirstString + " " + mySecondString;
var c = myFirstString + myFirstNumber;
var d = mySecondString - mySecondNumber;


// TODO Use console.log statements to print the values of the variables to the command line.
console.log(myFirstNumber);
console.log(myFirstBoolean);
console.log(mySecondNumber);
console.log(myFirstString);
console.log(mySecondString);

console.log(a);
console.log(b);
console.log(helloWorld);
console.log(c);
console.log(d);
