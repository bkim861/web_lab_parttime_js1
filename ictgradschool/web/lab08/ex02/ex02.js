"use strict";

// Provided variables
var amount = 3;
var userResponse = 'Y';
var firstName = "Bob";
var singer = "NOT Taylor Swift!";
var year = 1977;

// Example
var isOdd = (amount % 2 != 0);
console.log("isOdd = " + isOdd);

// TODO write your answers here
var answerY = (userResponse == "y" || userResponse == "Y");
console.log(answerY);
var nameBegin = (firstName.charAt(0) != "A");
console.log(nameBegin);
var realSinger = (singer != "Taylor Swift");
console.log(realSinger);
var yearBorn = (year > 1978 && year != 2013);
console.log(yearBorn);